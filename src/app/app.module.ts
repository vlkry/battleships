import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { WarshipcellComponent } from './warshipcell/warshipcell.component';
import { MatrixService } from './services/matrix/matrix.service';
import { ShipsPlacementService } from './services/ships-placement/ships-placement.service';
import { ConstantsService } from './services/constants/constants.service';


@NgModule({
  declarations: [
    AppComponent,
    WarshipcellComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    MatrixService,
    ShipsPlacementService,
    ConstantsService
 ],
  bootstrap: [AppComponent]
})
export class AppModule { }
