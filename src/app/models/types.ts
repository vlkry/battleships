export type RowLimiter = (row: number) => boolean;
export type ColLimiter = (col: number) => boolean;

export type TryFunc = (row: number, col: number, shouldPlaceShip: boolean) => boolean;
