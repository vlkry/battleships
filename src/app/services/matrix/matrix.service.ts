import { Injectable } from '@angular/core';
import { ConstantsService } from '../constants/constants.service';

@Injectable()
export class MatrixService {

  constructor(private constants: ConstantsService) { }

  public isMatrixInitiated(gameMatrix: number[][]): boolean {
    let res = gameMatrix !== []
              && gameMatrix.length === this.constants.END_ROW_NUMBER + 1;

    gameMatrix.forEach(row => {
      res = res && row.length === this.constants.END_COL_NUMBER + 1;
    });

    return res;
  }


  public initEmptyMatrix(gameMatrix: number[][]): void {
    for (let i = this.constants.START_ROW_NUMBER; i <= this.constants.END_ROW_NUMBER; i++) {
      gameMatrix[i] = [];
    }

    for (let i = this.constants.START_ROW_NUMBER; i <= this.constants.END_ROW_NUMBER; i++) {
      for (let j = this.constants.START_COL_NUMBER; j <= this.constants.END_COL_NUMBER; j++) {
        gameMatrix[i][j] = this.constants.EMPTY_CELL_VALUE;
      }
    }
  }
}
