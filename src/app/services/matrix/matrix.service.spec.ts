import { TestBed, inject } from '@angular/core/testing';

import { MatrixService } from './matrix.service';
import { ConstantsService } from '../constants/constants.service';

describe('MatrixService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MatrixService,
        ConstantsService
      ]
    });
  });

  it('should be created', inject([MatrixService], (service: MatrixService) => {
    expect(service).toBeTruthy();
  }));

  it('#initEmptyMatrix should init a matrix with empty values',
      inject([MatrixService, ConstantsService], (service: MatrixService, constants: ConstantsService) => {
      const matrix = [];
      service.initEmptyMatrix(matrix);
      expect(matrix[0][0]).toEqual(constants.EMPTY_CELL_VALUE);
    }));

  it('#initEmptyMatrix should not change matrix dimensions',
    inject([MatrixService, ConstantsService], (service: MatrixService, constants: ConstantsService) => {
    const matrix = [];
    service.initEmptyMatrix(matrix);
    expect(matrix[0][constants.END_COL_NUMBER + 1]).toBeUndefined();
    expect(matrix[constants.END_ROW_NUMBER + 1]).toBeUndefined();
  }));
});
