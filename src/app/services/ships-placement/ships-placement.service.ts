import { Injectable } from '@angular/core';
import { ConstantsService } from '../constants/constants.service';
import { MatrixService } from '../matrix/matrix.service';
import { TryFunc, RowLimiter, ColLimiter } from '../../models/types';

@Injectable()
export class ShipsPlacementService {
    constructor(private constants: ConstantsService, private matrixService: MatrixService) { }

    private gameMatrix: number[][] = [];
    public allShipsCells: [number, number][] = [];

    private placeNormalLShip = this.placeAnyShip(
        (row) => row <= this.constants.END_ROW_NUMBER - this.constants.L_SHIP_LENGTH,
        (col) => col <= this.constants.END_COL_NUMBER - this.constants.L_SHIP_WIDTH,
        this.tryPlaceNormalLShip);

    private placeLeftLShip = this.placeAnyShip(
        (row) => row >= this.constants.START_ROW_NUMBER + this.constants.L_SHIP_WIDTH,
        (col) => col <= this.constants.END_COL_NUMBER - this.constants.L_SHIP_LENGTH,
        this.tryPlaceLeftLShip);

    private placeRightLShip = this.placeAnyShip(
        (row) => row <= this.constants.END_ROW_NUMBER - this.constants.L_SHIP_WIDTH,
        (col) => col >= this.constants.START_COL_NUMBER + this.constants.L_SHIP_LENGTH,
        this.tryPlaceRightLShip);

    private placeReversedLShip = this.placeAnyShip(
        (row) => row >= this.constants.START_ROW_NUMBER + this.constants.L_SHIP_LENGTH,
        (col) => col >= this.constants.START_COL_NUMBER + this.constants.L_SHIP_WIDTH,
        this.tryPlaceReversedLShip);

    private placeVerticalIShip = this.placeAnyShip(
        (row) => row < this.constants.END_ROW_NUMBER - this.constants.I_SHIP_LENGTH,
        (col) => true,
        this.tryPlaceVerticalIShip);

    private placeHorizontalIShip = this.placeAnyShip(
        (row) => true,
        (col) => col < this.constants.END_COL_NUMBER - this.constants.I_SHIP_LENGTH,
        this.tryPlaceHorizontalIShip);

    private placeHorizontalTwoCellsShip = this.placeAnyShip(
        (row) => true,
        (col) => col <= this.constants.END_COL_NUMBER - this.constants.TC_SHIP_LENGTH,
        this.tryPlaceHorizontalTwoCellsShip);

    private placeVerticalTwoCellsShip = this.placeAnyShip(
        (row) => row <= this.constants.END_ROW_NUMBER - this.constants.TC_SHIP_LENGTH,
        (col) => true,
        this.tryPlaceVerticalTwoCellsShip);

    private placeSingleShip = this.placeAnyShip(
        (row) => true,
        (col) => true,
        this.tryPlaceSingleShip);

    public placeRandomLShip(gameMatrix: number[][]): void {
        if (!this.matrixService.isMatrixInitiated(gameMatrix)) {
            throw Error('Trying to place random LShip on not initiated game Matrix!');
        }
        this.gameMatrix = gameMatrix;
        const coinToss = Math.floor(Math.random() * 10) % 4;
        switch (coinToss) {
            case 0:
                this.placeNormalLShip();
                break;
            case 1:
                this.placeLeftLShip();
                break;
            case 2:
                this.placeRightLShip();
                break;
            case 3:
                this.placeReversedLShip();
                break;
            default:
                throw new Error('Wrong coin toss');
        }
    }

    public placeRandomIShip(gameMatrix: number[][]): void {
        if (!this.matrixService.isMatrixInitiated(gameMatrix)) {
            throw Error('Trying to place random IShip on not initiated game Matrix!');
        }
        this.gameMatrix = gameMatrix;
        const vertical = Math.floor(Math.random() * 10) % 2;
        if (vertical) {
            this.placeVerticalIShip();
        } else {
            this.placeHorizontalIShip();
        }
    }

    public placeRandomTwoCellsShip(gameMatrix: number[][]): void {
        if (!this.matrixService.isMatrixInitiated(gameMatrix)) {
            throw Error('Trying to place random two-cells Ship on not initiated game Matrix!');
        }
        this.gameMatrix = gameMatrix;
        const vertical = Math.floor(Math.random() * 10) % 2;
        if (vertical) {
            this.placeVerticalTwoCellsShip();
        } else {
            this.placeHorizontalTwoCellsShip();
        }
    }

    public placeRandomSingleShip(gameMatrix: number[][]): void {
        if (!this.matrixService.isMatrixInitiated(gameMatrix)) {
            throw Error('Trying to place random single Ship on not initiated game Matrix!');
        }
        this.gameMatrix = gameMatrix;
        this.placeSingleShip();
    }

    private placeShip(rowLimiter: RowLimiter, colLimiter: ColLimiter, tryFunc: TryFunc): void {
        const availableSlots = this.getAvailableSlotsSpaceForShip(rowLimiter, colLimiter, tryFunc);

        const slotIndex = Math.floor(Math.random() * availableSlots.length);

        tryFunc(availableSlots[slotIndex][0], availableSlots[slotIndex][1], true);
    }

    private placeAnyShip(rowLimiter: RowLimiter, colLimiter: ColLimiter, tryFunc: TryFunc) {
        const that = this;
        return function(): void {
            that.placeShip(rowLimiter, colLimiter, tryFunc.bind(that));
        };
    }

    private getAvailableSlotsSpaceForShip(rowLimiter: RowLimiter, colLimiter: ColLimiter, tryFunc: TryFunc): number[][] {
        const res = [];
        for (let i = 1; i < this.constants.VALID_ROWS.length; i++) {
            if (rowLimiter(i)) {
                for (let j = this.constants.VALID_COLS[0]; j < this.constants.VALID_COLS.length; j++) {
                    if (colLimiter(j) && tryFunc(i, j, false)) {
                        res.push([i, j]);
                    }
                }
            }
        }
        return res;
    }

    private tryPlaceNormalLShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j], [i + 1, j], [i + 2, j], [i + 2, j + 1]);
    }

    private tryPlaceLeftLShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j], [i, j + 1], [i, j + 2], [i - 1, j + 2]);
    }

    private tryPlaceRightLShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j], [i, j - 1], [i, j - 2], [i + 1, j - 2]);
    }

    private tryPlaceReversedLShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j], [i - 1, j], [i - 2, j], [i - 2, j - 1]);
    }

    private tryPlaceVerticalIShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j], [i + 1, j], [i + 2, j], [i + 3, j]);
    }

    private tryPlaceHorizontalIShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j], [i, j + 1], [i, j + 2], [i, j + 3]);
    }

    private tryPlaceVerticalTwoCellsShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j], [i + 1, j]);
    }

    private tryPlaceHorizontalTwoCellsShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j], [i, j + 1]);
    }
    private tryPlaceSingleShip(i: number, j: number, shouldPlaceShip: boolean): boolean {
        return this.tryPlaceAnyShip(shouldPlaceShip, [i, j]);
    }

    tryPlaceAnyShip(shouldPlaceShip: boolean, ...cells: [number, number][]): boolean {
        if (this.checkAvailability(...cells)) {
            if (shouldPlaceShip) {
                this.setShipValue(...cells);
                this.allShipsCells.push(...cells);
            }
            return true;
        }
        return false;
    }

    private setShipValue(...cells: [number, number][]): void {
        cells.forEach(cell => this.gameMatrix[cell[0]][cell[1]] = this.constants.SHIP_VALUE);
    }

    private checkAvailability(...cells: [number, number][]): boolean {
        return cells.every(cell => {
            return this.checkTopLeftCell(...cell)
                && this.checkTopCell(...cell)
                && this.checkTopRightCell(...cell)
                && this.checkLeftCell(...cell)
                && this.checkCurrentCell(...cell)
                && this.checkRightCell(...cell)
                && this.checkBottomLeftCell(...cell)
                && this.checkBottomCell(...cell)
                && this.checkBottomRightCell(...cell);
        });
    }

    private checkTopLeftCell(i: number, j: number): boolean {
        return !this.gameMatrix[i - 1][j - 1];
    }

    private checkTopCell(i: number, j: number): boolean {
        return !this.gameMatrix[i - 1][j];
    }

    private checkTopRightCell(i: number, j: number): boolean {
        return !this.gameMatrix[i - 1][j + 1];
    }

    private checkLeftCell(i: number, j: number): boolean {
        return !this.gameMatrix[i][j - 1];
    }

    private checkCurrentCell(i: number, j: number): boolean {
        return !this.gameMatrix[i][j];
    }

    private checkRightCell(i: number, j: number): boolean {
        return !this.gameMatrix[i][j + 1];
    }

    private checkBottomLeftCell(i: number, j: number): boolean {
        return !this.gameMatrix[i + 1][j - 1];
    }

    private checkBottomCell(i: number, j: number): boolean {
        return !this.gameMatrix[i + 1][j];
    }

    private checkBottomRightCell(i: number, j: number): boolean {
        return !this.gameMatrix[i + 1][j + 1];
    }
}
