import { TestBed, inject } from '@angular/core/testing';

import { ShipsPlacementService } from './ships-placement.service';
import { ConstantsService } from '../constants/constants.service';
import { MatrixService } from '../matrix/matrix.service';

describe('ShipsPlacementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ShipsPlacementService,
        MatrixService,
        ConstantsService
      ]
    });
  });

  it('should be created', inject([ShipsPlacementService], (service: ShipsPlacementService) => {
    expect(service).toBeTruthy();
  }));

  it('#placeRandomIShip should trow error on not initiated matrix',
    inject([ShipsPlacementService], (service: ShipsPlacementService) => {
      const matrix = [];

      expect(function () {
        service.placeRandomIShip(matrix);
      }).toThrowError('Trying to place random IShip on not initiated game Matrix!');
    }));

  it('#placeRandomIShip should set expected amount of cells',
    inject([ShipsPlacementService, MatrixService, ConstantsService],
      (service: ShipsPlacementService, matrixService: MatrixService, constants: ConstantsService) => {
        const matrix = [];
        matrixService.initEmptyMatrix(matrix);
        service.placeRandomIShip(matrix);

        let markedCellsCount = 0;
        matrix.forEach(row => {
          row.forEach(col => {
            if (col === constants.SHIP_VALUE) {
              markedCellsCount++;
            }
          });
        });

        expect(markedCellsCount).toEqual(constants.I_SHIP_LENGTH);
      }));

  it('#placeRandomIShip should not change any of border cells',
    inject([ShipsPlacementService, MatrixService, ConstantsService],
      (service: ShipsPlacementService, matrixService: MatrixService, constants: ConstantsService) => {
        const matrix = [];
        matrixService.initEmptyMatrix(matrix);

        service.placeRandomIShip(matrix);

        matrix.forEach((row, index) => {
          if (index === constants.START_ROW_NUMBER || index === constants.END_ROW_NUMBER) {
            row.forEach(col => {
              expect(col).toEqual(constants.EMPTY_CELL_VALUE);
            });
          } else {
            expect(row[constants.START_COL_NUMBER]).toEqual(constants.EMPTY_CELL_VALUE);
            expect(row[constants.END_COL_NUMBER]).toEqual(constants.EMPTY_CELL_VALUE);
          }
        });
      }));
});
