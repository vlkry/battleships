import { Injectable } from '@angular/core';

@Injectable()
export class ConstantsService {

  public EMPTY_CELL_VALUE = 0;
  public SHIP_VALUE = 1;
  public SHIP_KILLED_VALUE = 2;

  public VALID_ROWS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  public VALID_COLS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  public START_ROW_NUMBER = 0;
  public END_ROW_NUMBER = 11;

  public START_COL_NUMBER = 0;
  public END_COL_NUMBER = 11;

  public I_SHIP_LENGTH = 4;

  public L_SHIP_LENGTH = 3;
  public L_SHIP_WIDTH = 2;

  public TC_SHIP_LENGTH = 2;

  constructor() { }

}
