import { Component, OnInit } from '@angular/core';
import { MatrixService } from './services/matrix/matrix.service';
import { ShipsPlacementService } from './services/ships-placement/ships-placement.service';
import { ConstantsService } from './services/constants/constants.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Warships';
  restartBtnText = 'Restart';
  rows = [];
  columns = [];
  gameMatrix: number[][] = [];
  clicksMade = 0;


  constructor(
    private matrixService: MatrixService,
    private shipsPlacementService: ShipsPlacementService,
    private constants: ConstantsService
    ) {}

  ngOnInit(): void {
    this.rows = this.constants.VALID_ROWS;
    this.columns = this.constants.VALID_COLS;

    this.initMatrix();
  }

  initMatrix(): void {
    this.matrixService.initEmptyMatrix(this.gameMatrix);

    if (this.matrixService.isMatrixInitiated(this.gameMatrix)) {
      this.shipsPlacementService.placeRandomLShip(this.gameMatrix);
      this.shipsPlacementService.placeRandomIShip(this.gameMatrix);

      for (let i = 0; i < 2; i++) {
        this.shipsPlacementService.placeRandomTwoCellsShip(this.gameMatrix);
      }

      for (let i = 0; i < 4; i++) {
        this.shipsPlacementService.placeRandomSingleShip(this.gameMatrix);
      }
    }
  }

  cellClicked($event): void {
    this.clicksMade++;
    this.gameMatrix[$event[0]][$event[1]] = this.constants.SHIP_KILLED_VALUE;
    if (this.checkIfGameOver()) {
      setTimeout(fn => { alert('Great, You Won!!! It took you ' + this.clicksMade + ' shots to finish!'); this.restart(); }, 10);
    }
  }

  checkIfGameOver(): boolean {
    for (const [row, col] of this.shipsPlacementService.allShipsCells) {
      if (this.gameMatrix[row][col] === this.constants.SHIP_VALUE) {
        return false;
      }
    }
    return true;
  }

  restart(): void {
    window.location.reload();
  }
}
