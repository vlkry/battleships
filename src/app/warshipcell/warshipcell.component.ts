import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ValueTransformer } from '@angular/compiler/src/util';
import { ConstantsService } from '../services/constants/constants.service';

@Component({
  selector: 'app-warshipcell',
  templateUrl: './warshipcell.component.html',
  styleUrls: ['./warshipcell.component.css']
})
export class WarshipcellComponent implements OnInit {
  @Input()
  public matrixNumbers: number[];

  @Input()
  public value: number;

  @Output()
  clicked: EventEmitter<number[]> = new EventEmitter<number[]>();

  isTouched = false;
  isKillshot = false;

  constructor(private constants: ConstantsService) { }

  ngOnInit() {
  }

  cellClick() {
    if (!this.isTouched) {
      this.isTouched = true;
      if (this.value === this.constants.SHIP_VALUE) {
        this.isKillshot = true;
      }
      this.clicked.emit(this.matrixNumbers);
    }
  }
}
