import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarshipcellComponent } from './warshipcell.component';
import { ConstantsService } from '../services/constants/constants.service';

describe('WarshipcellComponent', () => {
  let component: WarshipcellComponent;
  let fixture: ComponentFixture<WarshipcellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarshipcellComponent ],
      providers: [ ConstantsService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarshipcellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
