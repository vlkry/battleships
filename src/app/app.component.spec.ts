import { TestBed, async, inject } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ConstantsService } from './services/constants/constants.service';
import { MatrixService } from './services/matrix/matrix.service';
import { ShipsPlacementService } from './services/ships-placement/ships-placement.service';
import { WarshipcellComponent } from './warshipcell/warshipcell.component';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        WarshipcellComponent
      ],
      providers: [
        ConstantsService,
        MatrixService,
        ShipsPlacementService
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Warships'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Warships');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to Warships!');
  }));
  it('should render restart in a div tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('div.restart-btn').textContent).toContain('Restart');
  }));
  it('should render expected amount of warshipcells', async(inject([ConstantsService], (constants: ConstantsService) => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelectorAll('app-warshipcell').length).toEqual( constants.VALID_COLS.length * constants.VALID_ROWS.length);
  })));

  const matrixValues = [[1, 1], [5, 5], [10, 10]];
  matrixValues.forEach(function(value) {
    it('should render warshipcell with a [' + value[0] + ', ' + value[1] + '] value of matrixNumbers', async(() => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;

      expect(compiled.querySelector('app-warshipcell[ng-reflect-matrix-numbers="1,1"]')).toBeTruthy();
    }));
  });


});
