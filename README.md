## Hello, this is Warships !


## How to run  the application:
Prerequisites: node package manager

    1) Navigate to the folder with this Repository
    2) Open console window or some other terminal there
    3) Run `npm install` command
    4) Run `ng serve` command
    5) Open `localhost:4200` in browser window to navigate to application
    6) Enjoy it!

## How to play:

    1) When the window is loaded you will see 10x10 battle board.
    2) Firstly all cells are marked grey and are untouched.
    3) You are able to click on cells to make your shots.
    4) If you hit ship with your shot the cell will become red.
    5) If you miss your shot the cell will become blue.
    6) When you hit all ships - you win! 
    7) Also you will see popup message with a number of hits it took you to hit all the ships.
    8) Let's see how close you can get to the score of 16?!

## Restart options:

In order to restart you can:

    1) Reload the page.
    2) Press restart button in the bottom left corner of the screen.

## Ships:

There are four kind of ships:

    1) 1-cell ships. Four 1-cell ships are located at the battlefield.
    2) 2-cell ships. Two 2-cell ships are located at the battlefield.
    3) 4-cell ships. There is one I-form ship (4 cells in a row) and one L-form ship.

## Ships Rules:

    1) All ships are randomly rotated.
    2) Ships cannot touch each other and are located at least 1 cell away from one another.